from tkinter import *
import random

ms_pool = {
    "Extremely Lucky" : 3,
    "Lucky" : 2,
    "Just Fine" : 1,
    "Be Careful!" : -1,
    "Unlucky" : -2,
    "OMG!" : -3
    }

fortune_score = 0

window = Tk()
window.title("Fortune Teller")
window.config(bg="beige")
window.geometry("550x500")

photoimage = PhotoImage(file="fortune_cookie.png")
canvas = Canvas(width=500, height=250, bg="beige", highlightthickness=0)
canvas.create_image(0, 0, image=photoimage, anchor=NW)
canvas.grid(row=2, column=0, columnspan=5)

message = Label(text="R U Lucky?", font=("Comic Sans MS", 24, "bold"), bg="white")
message.grid(row=2, column=3, columnspan=2, padx=(65, 0), pady=(0, 0))

fortune_score = 0
score_label = Label(text="Fortune Score: 0", font=("Comic Sans MS", 16), bg="beige")
score_label.grid(row=3, column=2, columnspan=3, pady=(10, 0))

def clicked():
    global fortune_score
    ms = random.choice(list(ms_pool.keys()))
    fortune_score += ms_pool[ms]
    message.config(text=ms)
    score_label.config(text=f"Fortune Score: {fortune_score}")

def reset_fortune():
    global fortune_score
    fortune_score = 0
    score_label.config(text="Fortune Score: 0")
    message.config(text="R U Lucky?")

button = Button(text="Click on me!", font=("Comic Sans MS", 16, "bold"), command=clicked, bg="red", bd=2, height=2, width=15)
button.grid(row=1, column=3,padx=(20,0), pady=(30, 0))

reset_button = Button(text="Reset Score", font=("Comic Sans MS", 16), command=reset_fortune, bg="red",bd=2, height=2, width=15)
reset_button.grid(row=1, column=4, pady=(30, 0))

window.mainloop()

